from spike_database.Coaches import Coaches
from spike_user_db.Users import Users

races = {
    "13": "Amazon",
    "24": "Bretonnian",
    "8": "Chaos Chosen",
    "21": "Chaos Dwarf",
    "9": "Dark Elf",
    "2": "Dwarf",
    "14": "Elf Union",
    "6": "Goblin",
    "11": "Halfling",
    "15": "High Elf",
    "1": "Human",
    "16": "Khemri",
    "5": "Lizardmen",
    "17": "Necromantic Horror",
    "12": "Norse",
    "18": "Nurgle",
    "19": "Ogre",
    "4": "Orc",
    "3": "Skaven",
    "25": "Slann",
    "10": "Undead",
    "22": "Underworld Denizens",
    "20": "Vampire",
    "7": "Wood Elf"
}


def get_naf_race(cyanide_race_id: int):
    return races.get(str(cyanide_race_id))


def get_naf_coach(coach_id: int, platform_id: int):
    user_db = Users()
    user_id = user_db.get_user_linked(coach_id, platform_id)
    naf_number = None
    naf_name = None
    if user_id is not None:
        user = user_db.get_user(user_id)
        if user is not None:
            if user.get("naf_number") not in [None, ""]:
                naf_number = user.get("naf_number")
            if user.get("naf_name") not in [None, ""]:
                naf_name = user.get("naf_name")
    return naf_number, naf_name
