import xml.etree.ElementTree as ElementTree
from datetime import datetime


class TourmaXmlReader:

    @staticmethod
    def read_tournament_file(filename: str):
        tree = ElementTree.parse(filename)
        root = tree.getroot()
        round_list = []
        for child in root:
            if child.tag == "Round":
                round_date = datetime.strptime(child.attrib.get("Date"), "%d/%m/%Y %H:%M:%S")
                round_data = {
                    "date": round_date.strftime("%Y-%m-%d %H:%M:%S"),
                    "matches": []
                }
                for match_node in child:
                    home_score = -1
                    away_score = -1
                    for value_node in match_node:
                        if value_node.attrib.get("Name") == "Touchdowns":
                            home_score = int(value_node.attrib.get("Value1", -1))
                            away_score = int(value_node.attrib.get("Value2", -1))
                    match_data = {
                        "home_coach": match_node.attrib.get("Coach1"),
                        "away_coach": match_node.attrib.get("Coach2")
                    }

                    if home_score != -1 and away_score != -1:
                        match_data["home_score"] = home_score
                        match_data["away_score"] = away_score
                    round_data["matches"].append(match_data)
                round_list.append(round_data)
        round_list = sorted(round_list, key=lambda i: i["date"])
        return round_list
