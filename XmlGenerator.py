from typing import List
from xml.etree.ElementTree import Element, SubElement, tostring

from xml.dom import minidom
from datetime import datetime

from spike_model.Match import Match
from spike_naf.Utilities import get_naf_race, get_naf_coach


class NafXml:

    @staticmethod
    def generate_from_tournament(matches: List[dict], organiser: str = "Spike"):
        coaches = []
        valid_match = []
        for match in matches:
            nb_naf = 0
            if match.get("home_team_data", {}).get("coach_id", 0) != 0:
                race = get_naf_race(match.get("home_team_data", {}).get("race_id"))
                if race is not None and match.get("home_team_data", {}).get("naf_number") is not None and match.get("home_team_data", {}).get("naf_name") is not None:
                    nb_naf += 1
                    coaches.append({
                        "name": match.get("home_team_data", {}).get("naf_name"),
                        "number": match.get("home_team_data", {}).get("naf_number"),
                        "team": race
                    })
            if match.get("away_team_data", {}).get("coach_id", 0) != 0:
                race = get_naf_race(match.get("away_team_data", {}).get("race_id"))
                if race is not None and match.get("away_team_data", {}).get("naf_number") is not None and match.get("away_team_data", {}).get("naf_name") is not None:
                    nb_naf += 1
                    coaches.append({
                        "name": match.get("away_team_data", {}).get("naf_name"),
                        "number": match.get("away_team_data", {}).get("naf_number"),
                        "team": race
                    })
            if nb_naf == 2:
                valid_match.append(match)

        coaches = list({v['number']: v for v in coaches}.values())

        root = Element("nafReport")
        root.set("xmlns:blo", "https://www.spike.ovh")
        organiser_elem = SubElement(root, "organiser")
        organiser_elem.text = organiser

        coaches_elem = SubElement(root, "coaches")
        for coach in coaches:
            coach_elem = SubElement(coaches_elem, "coach")
            sub_elem = SubElement(coach_elem, "name")
            sub_elem.text = coach["name"]
            sub_elem = SubElement(coach_elem, "number")
            sub_elem.text = str(coach["number"])
            sub_elem = SubElement(coach_elem, "team")
            sub_elem.text = coach["team"]

        for match in valid_match:
            game_elem = SubElement(root, "game")
            time_stamp_elem = SubElement(game_elem, "timeStamp")
            time_stamp_elem.text = match.get("date", datetime.utcnow().strftime("%Y-%m-%d %H:%M"))

            # home team
            player_record_elem = SubElement(game_elem, "playerRecord")
            sub_elem = SubElement(player_record_elem, "name")
            sub_elem.text = str(match.get("home_team_data", {}).get("naf_name")).strip()
            sub_elem = SubElement(player_record_elem, "number")
            sub_elem.text = str(match.get("home_team_data", {}).get("naf_number")).strip()
            sub_elem = SubElement(player_record_elem, "teamRating")
            sub_elem.text = "110"
            sub_elem = SubElement(player_record_elem, "touchDowns")
            sub_elem.text = str(match.get("home_team", {}).get("score")).strip()
            sub_elem = SubElement(player_record_elem, "badlyHurt")
            sub_elem.text = str(match.get("home_team", {}).get("casualties")).strip()

            # away team
            player_record_elem = SubElement(game_elem, "playerRecord")
            sub_elem = SubElement(player_record_elem, "name")
            sub_elem.text = str(match.get("away_team_data", {}).get("naf_name")).strip()
            sub_elem = SubElement(player_record_elem, "number")
            sub_elem.text = str(match.get("away_team_data", {}).get("naf_number")).strip()
            sub_elem = SubElement(player_record_elem, "teamRating")
            sub_elem.text = "110"
            sub_elem = SubElement(player_record_elem, "touchDowns")
            sub_elem.text = str(match.get("away_team", {}).get("score")).strip()
            sub_elem = SubElement(player_record_elem, "badlyHurt")
            sub_elem.text = str(match.get("away_team", {}).get("casualties")).strip()

        xml_str = tostring(root, "utf-8")
        xml_str = minidom.parseString(xml_str)
        return xml_str.toprettyxml(indent="  ")

    @staticmethod
    def generate_from_matches(matches: List[Match], organiser: str = "Spike"):
        coaches = []
        valid_match = []
        for match in matches:
            print(match.get_competition_name())
            nb_naf = 0
            if match.get_coach_home().get_id() != 0:
                naf_number, naf_name = get_naf_coach(match.get_coach_home().get_id(), match.get_platform_id())
                race = get_naf_race(match.get_team_home().get_race_id())
                if naf_number is not None and naf_name is not None:
                    nb_naf += 1
                    coaches.append({
                        "name": naf_name,
                        "number": naf_number,
                        "team": race
                    })
            if match.get_coach_away().get_id() != 0:
                naf_number, naf_name = get_naf_coach(match.get_coach_away().get_id(), match.get_platform_id())
                race = get_naf_race(match.get_team_away().get_race_id())
                if naf_number is not None and naf_name is not None:
                    nb_naf += 1
                    coaches.append({
                        "name": naf_name,
                        "number": naf_number,
                        "team": race
                    })
            if nb_naf == 2:
                valid_match.append(match)

        coaches = list({v['number']: v for v in coaches}.values())

        root = Element("nafReport")
        root.set("xmlns:blo", "https://www.spike.ovh")
        organiser_elem = SubElement(root, "organiser")
        organiser_elem.text = organiser

        coaches_elem = SubElement(root, "coaches")
        for coach in coaches:
            coach_elem = SubElement(coaches_elem, "coach")
            sub_elem = SubElement(coach_elem, "name")
            sub_elem.text = coach["name"]
            sub_elem = SubElement(coach_elem, "number")
            sub_elem.text = str(coach["number"])
            sub_elem = SubElement(coach_elem, "team")
            sub_elem.text = coach["team"]

        for match in valid_match:
            game_elem = SubElement(root, "game")
            time_stamp_elem = SubElement(game_elem, "timeStamp")
            time_stamp_elem.text = match.get_start_datetime("%Y-%m-%d %H:%M")

            # home team
            naf_number, naf_name = get_naf_coach(match.get_coach_home().get_id(), match.get_platform_id())
            player_record_elem = SubElement(game_elem, "playerRecord")
            sub_elem = SubElement(player_record_elem, "name")
            sub_elem.text = naf_name
            sub_elem = SubElement(player_record_elem, "number")
            sub_elem.text = str(naf_number)
            sub_elem = SubElement(player_record_elem, "teamRating")
            sub_elem.text = "110"
            sub_elem = SubElement(player_record_elem, "touchDowns")
            sub_elem.text = str(match.get_score_home())
            sub_elem = SubElement(player_record_elem, "badlyHurt")
            sub_elem.text = str(match.get_inflicted_casualties_home())

            # away team
            naf_number, naf_name = get_naf_coach(match.get_coach_away().get_id(), match.get_platform_id())
            player_record_elem = SubElement(game_elem, "playerRecord")
            sub_elem = SubElement(player_record_elem, "name")
            sub_elem.text = naf_name
            sub_elem = SubElement(player_record_elem, "number")
            sub_elem.text = str(naf_number)
            sub_elem = SubElement(player_record_elem, "teamRating")
            sub_elem.text = "110"
            sub_elem = SubElement(player_record_elem, "touchDowns")
            sub_elem.text = str(match.get_score_away())
            sub_elem = SubElement(player_record_elem, "badlyHurt")
            sub_elem.text = str(match.get_inflicted_casualties_away())

        xml_str = tostring(root, "utf-8")
        xml_str = minidom.parseString(xml_str)
        return xml_str.toprettyxml(indent="  ")
